package com.m3.training.helloworldmsg;


import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class HelloWorldMessageTest {
	
	static private HelloWorldMessage objectUnderTest;
	
	@BeforeAll
	static void setupBeforeClass() {
		objectUnderTest = new HelloWorldMessage();
	}

	@Test
	void test_newInstance() {
		assertNotNull(objectUnderTest);
	}

	@Test
	void test_getMessage() {
		assertNotNull(objectUnderTest.getMessage());
	}

	@Test
	void test_getMessage_twoUniqueMessage() {
		String firstResult = objectUnderTest.getMessage();
		String secondResult = objectUnderTest.getMessage();
		String errorMessage = 
				"Object under test could not produce two unique results."
				+ " [" + firstResult + "] "
				+ " [" + secondResult + "] ";
		System.out.println(errorMessage);

		assertNotEquals(firstResult, secondResult, errorMessage);
	}
	
}
